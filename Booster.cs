using System.Linq;
using UnityEngine;

namespace SampleCode
{
    /// <summary>
    /// Booster's passively change the unit's behaviour
    /// Create a booster by putting this class into a gameobject, and creating the effects of needed for the booster
    /// as children for that gameobject. Each booster has upgrades that the player can choose from.
    /// </summary>
     public class Booster: MonoBehaviour
     {
         [Header("Booster Information")] 
         public Sprite Image;
         public string Name;
         public string BoosterDescription;

         public Effect[] Effects;

         public void ActivateEffect(EffectTier tier)
         {
             var matchingTier = Effects.FirstOrDefault(effect => effect.Tier == tier);
             
             if (matchingTier != null)
             {
                 matchingTier.RegisterOnEvents();
             }
             else
             {
                 Debug.LogWarning("Tried to activate booster " + Name + " but it does does not have " + tier  + "defined" );
             }
         }

         public void DeactivateEffect(EffectTier tier)
         {
             var matchingTier = Effects.FirstOrDefault(effect => effect.Tier == tier);

             if (matchingTier != null)
             {
                 matchingTier.RegisterOnEvents();
             }
             else
             {
                 Debug.LogWarning("Tried to deactivate booster " + Name + " but it does does not have " + tier  + "defined" );
             }
         }
     }
 }
