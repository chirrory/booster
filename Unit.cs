using UnityEngine;

namespace SampleCode
{
    /// <summary>
    /// This is a simplified version of the unit class, which all the units in the game derive from.
    /// </summary>
    public  class Unit : MonoBehaviour
    {
        public TypeName TypeName;

        //In the original solution, the unit kept a collection of it's effects,
        //but I removed it for this code sample for simplicity 
        public Modifier AddEffect(Modifier modifierPrefab)
        {
            var effect = Instantiate(modifierPrefab.gameObject, transform);
            return effect.GetComponent<Modifier>();
        }
        
    }

    public enum TypeName
    {
        Troll,
        Wolf,
        Elf,
        Human
    }
}