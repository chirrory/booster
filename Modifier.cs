using UnityEngine;

namespace SampleCode
{
    /// <summary>
    /// Modifiers directly change their parent unit's behaviour, and they exist for the time that the parent unit exists.
    /// </summary>
    public abstract class Modifier : MonoBehaviour
    {
        public Unit AffectedUnit
        {
            get { return GetComponentInParent<Unit>(); }
        }
        public abstract void Activate();
    }
    
}