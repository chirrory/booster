using UnityEngine;

namespace SampleCode
{
    /// <summary>
    /// Effect is a collection of modifiers, that attach to the unit's to change the behaviour.
    /// Effect registers on the OnUnitSpawned event, which gets triggered when a new unit spawns.
    /// Then it registers itself to the unit and gets activateds
    /// </summary>
    public class Effect : MonoBehaviour
    {
        public EffectTier Tier
        private Unit AffectedUnit;

        public Modifier[] ModifierPrefabs;

        private void FilterUnit(Unit unit)
        {
            if (unit.TypeName != AffectedUnit.TypeName)
            {
                return;
            }
            
            foreach (Modifier modifier in ModifierPrefabs)
            {
                var modifierInstance = unit.AddEffect(modifier);
                modifierInstance.Activate();
            }

        }
    
        public void RegisterOnEvents()
        {
            UnitSpawner.OnUnitSpawned += FilterUnit;
        }

        public void DeregisterOnEvents()
        {
            UnitSpawner.OnUnitSpawned -= FileterUnit;
        }
    }

    public enum EffectTier
    {
        TierOne,
        TierTwo,
        TierThree,
        TierFour
    }
    
}