namespace SampleCode.Modifiers
{
    public class UnitSpeedModifier : Modifier
    {
        public override void Activate()
        {
            //Here we can write the actual code to affect the unit's speed components. 
        }
    }
}