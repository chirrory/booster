This code won't compile, and it's for showing how my code would be in a real project.

Boosters are passive effects that affect unit's behaviour. The player can choose choose which boosters they want to use in the level before the level begins. 
Each booster can have up to four tiers, which are essentially upgrades to the booster's effects. Each tier of a booster is defined by the Effect class. Each effect 
has a collection of modifiers, which are prefabs containing a script that inherits the Modifier class and define the modifier's behaviour. These modifiers can be things like affect the unit's speed,
health, or basically whatever the designer wants to affect with the booster. 

Once instantiated, the modifiers exists for the lifetime of the unit, and get destroyed at the point the unit gets destroyed. 

The modifier folder contains one modifier to show where the modifier would be, but it's not been implemented. 